﻿using UnityEditor;
using UnityEngine;
using System.Xml;
using EasyARBundleTool;

namespace EasyARBundleTool
{
    public class AutoBuildUtils
    {
        static void BuildBundle()
        {
            int  errorCode = 0;

            CommandLineArgsParser unity3dArgsParser = new CommandLineArgsParser("-");
            CommandLineArgsParser customizedArgsParser = new CommandLineArgsParser("+");

            Debug.Log("Command line args: ");
            for (int i = 0; i < unity3dArgsParser.GetAllArgs().Length; i++)
            {
                Debug.Log("unity3d arg : " + unity3dArgsParser.GetAllArgs()[i]);
            }

            for (int i = 0; i < customizedArgsParser.GetAllArgs().Length; i++)
            {
                Debug.Log("customized arg : " + customizedArgsParser.GetAllArgs()[i]);
            }

            string projectPath = "";
            unity3dArgsParser.GetArgString("projectPath", out projectPath);

            // find BundleConfig.xml
            string bundleConfigXmlPath = GetBundleConfigXmlPath();
            Debug.Log("BundleConfig.xml path is : " + bundleConfigXmlPath);

            bundleConfigXmlPath = System.IO.Path.Combine(projectPath, bundleConfigXmlPath);
            Debug.Log("BundleConfig.xml full path is : " + bundleConfigXmlPath);

            // get root prefab path
            string bundleRootPrefabPath = GetBundleRootPrefabPath(bundleConfigXmlPath);
            Debug.Log("BundleRootPrefab full path is : " + bundleRootPrefabPath);

            // build bundle;
            if (bundleRootPrefabPath != null)
            {
                Object[] objects = AssetDatabase.LoadAllAssetRepresentationsAtPath(bundleRootPrefabPath);
                if (objects.Length != 0)
                {
                    // build bundles
                    BuildBundles.BuildBundleAll(objects);

                    // copy to dest path

                    // upload

                }
                else
                {
                    Debug.Log("Load bundle root prefab failed : " + bundleRootPrefabPath);
                    errorCode = 1;
                }
            }

            EditorApplication.Exit(errorCode);
            
        }

        static string GetBundleConfigXmlPath()
        {
            string bundleConfigXmlPath = null;
#if UNITY_4_6
            string[] allAssetsPath = AssetDatabase.GetAllAssetPaths();
            for (int i = 0; i < allAssetsPath.Length; i++)
            {
                string path = allAssetsPath[i];
                if (System.IO.Path.GetFileName(path) == "BundleConfig.xml")
                {
                    bundleConfigXmlPath = path;
                    break;
                }
            }
#else
            
#endif

            return bundleConfigXmlPath;
        }

        static string GetBundleRootPrefabPath(string bundleConfigXmlPath)
        {
            string bundleRootPrefabPath = null;

            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(bundleConfigXmlPath);

                XmlNodeList nodeList = xmlDoc.ChildNodes;
                for (int i = 0; i < nodeList.Count; i++)
                {
                    XmlNode curNode = nodeList[i];
                    string key = curNode.Name;
                    string val = curNode.InnerText;

                    if (key == "BundleRootPath")
                    {
                        bundleRootPrefabPath = val;
                    }
                }
            }
            catch
            {
                Debug.LogError("Read bundle root prefab path failed");
            }

            return bundleRootPrefabPath;
        }
    }
}
